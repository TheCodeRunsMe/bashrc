# .bashrc for GNU/Linux
Fancy `.bashrc`  customisation

This is a simple script to customise the [non-login shell](https://www.gnu.org/software/bash/manual/bash.html#Invoking-Bash).



\# sign can be used for commenting in bash scripts so if you do not want a customisation, edit ~/.bashrc file: simply place a \# sign to the beginning of the customisation line.

Check `cv` function out, you will love it.  

## Dependencies (All optional)
* `cowthink`
* `bash-completion`
* `rsync`
* `cmatrix`
* `screenfetch`
* `doge`
* `fortune-mod`
* `toilet`
* `figlet`

## Setup
Back up you previous `.bashrc` file:

```bash
cp ~/.bashrc ~/.bashrc.b
```

From terminal go to the directory you downloaded the `.bashrc` file. Copy the new `.bashrc` file to the home directory:

```bash
cp .bashrc ~/.bashrc
```
*A view of startup*

![Terminal in Tilix](https://s33.postimg.cc/4jlx3m3i7/Screenshot_20180715_005423.png)

